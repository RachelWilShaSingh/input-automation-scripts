import pyautogui
import time

mouseX, mouseY = pyautogui.position()
print( "Mouse is at:", mouseX, mouseY )

buttonX = 1863
buttonY = 745
print( "Button is at:", buttonX, buttonY )

# Get ready
print( "Prepare..." )
pyautogui.moveTo( buttonX, buttonY )
time.sleep( 1 )

# Keep synthesizing
print( "Loop..." )
while ( True ):
	pyautogui.click() # Click Synthesize button
	time.sleep( 4 )
	pyautogui.click() # Click the synthesis button
	time.sleep( 4 )
