import sys
import time
import os
import random
import datetime
import json
import pyautogui
import pygame
from pygame.locals import *

from ui import *

# I need to add "collecting" as a thing that happens at intervals and then separate logic for "reconstructing" shit.

# Just so it looks nicer...
secondsPerMinute = 60;  minutesPerHour = 60


class GameAutomater:
    def __init__( self ):
        self.minutes        = 0
        self.window         = None
        self.bgColor        = None
        self.textColor      = None
        self.font           = None
        self.pingSound      = None
        self.startTicks     = None
        self.timeOffset     = 0
        self.timerText      = None
        self.screenWidth    = None
        self.mountPoint     = (0, 0)
        self.initializing   = True
        self.paused         = False
        self.status         = "Waiting"
        self.buttonPos      = (0, 0)
        self.screenWidth    = int(1920/4)
        self.screenHeight   = 1080
        self.fontSize       = 10
        self.windowX        = -100
        self.windowY        = 0
        
        self.buttons        = {}
        self.labels         = {}
        self.logLabels      = {}
        self.logButtons     = {}


    def Run( self ):
        self.Log( "main", "" )
        self.Log( "main", "ARGUMENTS" )
        for i in range( len( sys.argv ) ):  self.Log( "main", str( i ) + ": " + sys.argv[i] + "\t" )
        if ( len( sys.argv ) >= 2 ):    self.timeOffset = int( sys.argv[1] )
        else:                           self.timeOffset = 0

        self.ClearData()
        self.PygameInit()
        self.BuildUI()
        self.LoadData()
        self.ProgramLoop()
        
        
    def DrawUI( self ):
        for button in self.buttons.values():
            button.Draw( self.window )
            
        for label in self.labels.values():
            label.Draw( self.window )
            
        for button in self.logButtons.values():
            button.Draw( self.window )
            
        for label in self.logLabels.values():
            label.Draw( self.window )
    
    def ResetLocation( self ):
        # Click in blank space to avoid accidentally clicking dialogs I don't mean to
        self.Log( "reset", "Reset location" )
        self.MoveMouseAbsoluteAndClick( 98, 315 )


    def ClearData( self ):
        self.Log( "clear", "Clear location data..." )
        self.timingInfo = {
            "5m" : [],
            "15m" : [],
            "4h" : [],
            "8h" : []
        }
        
        self.ClearTableLog()
        
        
    def ClearTableLog( self ):
        self.logLabels      = {}
        self.logButtons     = {}
    
    
    def DeleteItem( self, itemType, index, timespan ):
        index = int( index )
        
        # Remove from the data object
        del self.timingInfo[timespan][index]
        
        self.BuildTableFromData()


    def BuildTableFromData( self ):
        self.ClearTableLog()
        
        # Populate table
        counter = 0
        for timespan in self.timingInfo.keys():            
            for item in self.timingInfo[timespan]:
                itemType = "Col"
                if ( item["action"] == "generate" ):
                    itemType = "Gen"
                    
                pos = item["pos"]
                button = (0,0)
                if ( "button" in item ):
                    button = item["button"]
            
                self.CreateTableLog( itemType, timespan, pos, button, counter )
                counter += 1
                

    def CreateTableLog( self, itemType, timespan, mousePos, buttonPos, counter = None ):
        if ( counter == None ):
            index = len( self.timingInfo[timespan] ) - 1
        else:
            index = counter
            
        indexStr = str( index )
        posStr = "(" + str( mousePos[0] ) + "," + str( mousePos[1] ) + ")"
        genStr = ""
        if ( itemType == "Gen" ):
            genStr = "(" + str( buttonPos[0] ) + "," + str( buttonPos[1] ) + ")"
        inc = 30
        
        # Create label and delete button
        adjIndex = index + 1
        
        columnPos = self.labels["lblIdx"].rect
        self.logLabels[itemType + indexStr + "Idx"]   = ( Label ( { "rect" : ( columnPos[0], columnPos[1] + ( adjIndex * inc ), 0, 0        ), "font" : self.font, "text" : indexStr } ) )
        
        columnPos = self.labels["lblPos"].rect
        self.logLabels[itemType + indexStr + "Pos"]   = ( Label ( { "rect" : ( columnPos[0], columnPos[1] + ( adjIndex * inc ), 0, 0        ), "font" : self.font, "text" : posStr } ) )
        
        columnPos = self.labels["lblInt"].rect
        self.logLabels[itemType + indexStr + "Int"]   = ( Label ( { "rect" : ( columnPos[0], columnPos[1] + ( adjIndex * inc ), 0, 0        ), "font" : self.font, "text" : timespan } ) )
        
        columnPos = self.labels["lblType"].rect
        self.logLabels[itemType + indexStr + "Type"]  = ( Label ( { "rect" : ( columnPos[0], columnPos[1] + ( adjIndex * inc ), 0, 0        ), "font" : self.font, "text" : itemType } ) )
        
        if ( itemType == "Gen" ):
            columnPos = self.labels["lblGen"].rect
            self.logLabels[itemType + indexStr + "Gen"]  = ( Label ( { "rect" : ( columnPos[0], columnPos[1] + ( adjIndex * inc ), 0, 0        ), "font" : self.font, "text" : genStr } ) )
        
        # Delete button
        columnPos = self.labels["lblDel"].rect
        self.logButtons["btnDelete" + itemType + indexStr]           = ( Button( { "rect" : ( columnPos[0], columnPos[1] + ( adjIndex * inc ), 60, 20     ), "font" : self.font, "text" : "x",       "action" : "delete_" + itemType + "_" + indexStr + "_" + timespan } ) )


    def RecordCollection( self, action, timespan, mousePos ):
        self.Log( "record", "Record action \"" + action + "\" with timespan " + timespan + " at position " + str( mousePos ) )
        self.timingInfo[timespan].append( { "action" : action, "pos" : mousePos, "lastTriggered" : 0 } )
        
        #self.CreateTableLog( "Col", timespan, mousePos )
        self.BuildTableFromData()
        
 
    def RecordGeneration( self, action, timespan, mousePos, buttonPos ):
        self.Log( "record", "Record action \"" + action + "\" with timespan " + timespan + " at position " + str( mousePos ) )
        self.timingInfo[timespan].append( { "action" : action, "pos" : mousePos, "button" : buttonPos, "lastTriggered" : 0 } )
        
        #self.CreateTableLog( "Gen", timespan, mousePos )
        self.BuildTableFromData()
             
    
    def TimespanToInt( self, timestr ):
        if      ( timestr == "5m" ):   return 5+1
        elif    ( timestr == "15m" ):  return 15+1
        elif    ( timestr == "4h" ):   return 4*60+1
        elif    ( timestr == "8h" ):   return 8*60+1
        return 0
        

    def RunActions( self ):
        if ( self.GetElapsedMinutes() <= 0 ): return
        if ( self.paused ): return
        
        for timespan in self.timingInfo:
            for index in range( len( self.timingInfo[timespan] ) ):
                if   ( self.timingInfo[timespan][index]["action"] == "collect" ):     
                    if ( self.RunCollection( index, timespan ) ):
                        # Update last collected
                        self.timingInfo[timespan][index]["lastTriggered"] = self.GetElapsedMinutes() 
                    
                elif ( self.timingInfo[timespan][index]["action"] == "generate" ):    
                    if ( self.RunGeneration( index, timespan ) ):
                        # Update last collected
                        self.timingInfo[timespan][index]["lastTriggered"] = self.GetElapsedMinutes()
        
        
    def RunCollection( self, index, timespan ):
        if ( self.paused ): return
        item = self.timingInfo[timespan][index]
        
        timespanInt = self.TimespanToInt( timespan )                    
        timeSinceLastCollect = self.GetElapsedMinutes() - item["lastTriggered"]
        haventAlreadyCollectedIt = ( timeSinceLastCollect >= timespanInt )
        
        if ( haventAlreadyCollectedIt ):
        
            self.Log( "collect", "Time: " + str( timespanInt ) )
            self.Log( "collect", "Since last collect: " + str( timeSinceLastCollect ) )
            
            self.Log( "actions", item["action"] + " @ " + str( item["pos"] ) + "..." )
            self.pingSound.play() # Warn me that you're going to move my dang mouse
            time.sleep( 2.0 )
            self.MoveMouseAbsoluteAndClick( item["pos"][0], item["pos"][1] )
            
            return True  # Updates
        
        return False    # No updates
                 
            
            
    def RunGeneration( self, index, timespan ): 
        if ( self.paused ): return       
        # Collect this item first
        if ( self.RunCollection( index, timespan ) ):
            # Now generate new
            item = self.timingInfo[timespan][index]
            
            timespanInt = self.TimespanToInt( timespan )                    
            timeSinceLastCollect = self.GetElapsedMinutes() - item["lastTriggered"]
            haventAlreadyCollectedIt = ( timeSinceLastCollect >= timespanInt )
            
            if ( haventAlreadyCollectedIt ):
                # Click on the building again
                self.MoveMouseAbsoluteAndClick( item["pos"][0], item["pos"][1] )
                
                # Click on the generation button
                self.MoveMouseAbsoluteAndClick( item["button"][0], item["button"][1] )
                
                # Close any open dialogs before continuing
                pyautogui.press( "esc" )
                
                return True  # Updates
        
        return False    # No updates
         
         
    def PositionToTuple( self, pos ):
        return ( pos.x, pos.y )
    
    
    def DeselectTimespanButtons( self ):
        self.buttons["btn5m"].Deselect()
        self.buttons["btn15m"].Deselect()
        self.buttons["btn4h"].Deselect()
        self.buttons["btn8h"].Deselect()
        
        
    def SelectTimespanButton( self, timespan ):
        self.DeselectTimespanButtons()
        self.buttons["btn" + timespan].Select()
        self.status = timespan + " collections"
        self.inputTime = timespan
         
         
    def ProgramLoop( self ):
        #self.initializing = True
        #self.Initialize()
        self.initializing = False
        self.inputTime = "5m"
        self.buttonType = "Supplies"
        self.buttonPos = self.PositionToTuple( pyautogui.position() )
                    
        # Program loop
        while True:
            self.window.fill( self.bgColor )
                        
            self.labels["lblTimer"].text = str( int( self.GetElapsedMinutes() ) + self.timeOffset ) + ":" + str( int( self.GetElapsedSeconds() % 60 ) )
            self.labels["buttonPos"].text = str( self.buttonPos[0] ) + "," + str( self.buttonPos[1] )
            self.labels["inputTime"].text = self.inputTime
            
            self.DrawUI()

            for event in pygame.event.get():
                if event.type == QUIT:
                    self.SaveData()
                    pygame.quit()
                    sys.exit()
                    
                elif event.type == MOUSEBUTTONDOWN:
                    for button in self.buttons.values():
                        if ( button.IsClicked( event.pos ) ):                            
                            if ( "timespan" in button.action ):
                                data = button.action.split("_")
                                self.SelectTimespanButton( data[1] )
                                
                    for button in self.logButtons.values():
                        if ( button.IsClicked( event.pos ) ):
                            if ( "delete" in button.action ):
                                data = button.action.split("_")                                
                                self.DeleteItem( data[1], data[2], data[3] )

                elif event.type == KEYDOWN:
                    if      ( event.key == K_1 ): self.SelectTimespanButton( "5m" )
                    elif    ( event.key == K_2 ): self.SelectTimespanButton( "15m" )
                    elif    ( event.key == K_3 ): self.SelectTimespanButton( "4h" )
                    elif    ( event.key == K_4 ): self.SelectTimespanButton( "8h" )
                        
                    elif ( event.key == K_q ):
                        self.status = "Button position set"
                        self.buttonPos = self.PositionToTuple( pyautogui.position() )
                        
                    elif ( event.key == K_0 ):
                        self.timeOffset = 5
                        self.RunActions()
                        
                    elif ( event.key == K_a ):
                        self.status = "Record collection"
                        self.RecordCollection( "collect", self.inputTime, self.PositionToTuple( pyautogui.position() ) )
                        
                    elif ( event.key == K_s ):
                        self.status = "Record generation"
                        self.RecordGeneration( "generate", self.inputTime, self.PositionToTuple( pyautogui.position() ), self.buttonPos )

                    elif event.key == K_z:
                        self.status = "Mouse position is " + str( pos.x ) + "," + str( pos.y )
                        pos = pyautogui.position()

                    elif event.key == K_l:
                        self.status = "Cleared data"
                        self.ClearData()
                        
                    elif event.key == K_p:
                        self.paused = not self.paused
                        if self.paused:
                            self.status = "Paused"
                        else:
                            self.status = "Running"
                        

                    elif event.key == K_d:
                        self.DryRun()

                    elif event.key == K_g:
                        self.MoveMouseAbsolute( self.mountPoint[0], self.mountPoint[1] )

                    elif event.key == K_h:
                        self.SetMountPoint( pyautogui.position() )

                    elif event.key == K_EQUALS:
                        self.timeOffset += 1
                        self.UpdateTimerText( ( self.screenWidth - 50, 0 ) )

                    elif event.key == K_MINUS:
                        self.timeOffset -= 1
                        self.UpdateTimerText( ( self.screenWidth - 50, 0 ) )

            pygame.display.update()
            self.fps.tick( 30 )
            
            
            if ( self.initializing ):
                self.InitializeView()
            else:
                self.RunActions()
            
         
    def BuildUI( self ):
        px = 5
        py = 5
        colWidth = self.screenWidth / 8
        rowHeight = self.screenHeight / 55
        
        self.labels["lblTimespan"]      = ( Label ( { "rect" : ( px, py+rowHeight*0, 0, 0        ), "font" : self.font, "text" : "Timespan" } ) )
        self.buttons["btn5m"]           = ( Button( { "rect" : ( px, py+rowHeight*1, 60, 20     ), "font" : self.font, "text" : "[1] 5m",       "action" : "timespan_5m", "selected" : True } ) )
        self.buttons["btn15m"]          = ( Button( { "rect" : ( px, py+rowHeight*2, 60, 20    ), "font" : self.font, "text" : "[2] 15m",      "action" : "timespan_15m" } ) )
        self.buttons["btn4h"]           = ( Button( { "rect" : ( px, py+rowHeight*3, 60, 20   ), "font" : self.font, "text" : "[3] 4h",       "action" : "timespan_4h" } ) )
        self.buttons["btn8h"]           = ( Button( { "rect" : ( px, py+rowHeight*4, 60, 20   ), "font" : self.font, "text" : "[4] 8h",       "action" : "timespan_8h" } ) )
        self.labels["lblButtonPos"]     = ( Label ( { "rect" : ( px, py+rowHeight*6, 0, 0        ), "font" : self.font, "text" : "Button position" } ) )
        self.labels["lblButtonPos2"]    = ( Label ( { "rect" : ( px, py+rowHeight*7, 0, 0        ), "font" : self.font, "text" : "[Q] - Set" } ) )
        self.labels["buttonPos"]        = ( Label ( { "rect" : ( px, py+rowHeight*8, 0, 0       ), "font" : self.font, "text" : "x" } ) )
        self.labels["inputTime"]        = ( Label ( { "rect" : ( px, py+rowHeight*9, 0, 0        ), "font" : self.font, "text" : "0" } ) )
        self.labels["lblLogger"]        = ( Label ( { "rect" : ( px, py+rowHeight*11, 0, 0        ), "font" : self.font, "text" : "Log action" } ) )
        self.labels["lblLogger2"]       = ( Label ( { "rect" : ( px, py+rowHeight*12, 0, 0       ), "font" : self.font, "text" : "[A] - Collect" } ) )
        self.labels["lblLogger3"]       = ( Label ( { "rect" : ( px, py+rowHeight*13, 0, 0       ), "font" : self.font, "text" : "[S] - Generate" } ) )
        self.labels["lblLogger4"]       = ( Label ( { "rect" : ( px, py+rowHeight*14, 0, 0       ), "font" : self.font, "text" : "[P] - Pause" } ) )
        self.labels["lblLogger5"]       = ( Label ( { "rect" : ( px, py+rowHeight*15, 0, 0       ), "font" : self.font, "text" : "[O] - Dry run" } ) )
        self.labels["lblLogger6"]       = ( Label ( { "rect" : ( px, py+rowHeight*16, 0, 0      ), "font" : self.font, "text" : "[L] - Clear data" } ) )
        self.labels["lblTimer"]         = ( Label ( { "rect" : ( px, py+rowHeight*18, 0, 0        ), "font" : self.font, "text" : "0" } ) )
        
        tableX = px + colWidth * 1
        tableY = py + rowHeight*0
        self.labels["lblIdx"]        = ( Label ( { "rect" : ( px+colWidth*2, tableY, 0, 0        ), "font" : self.font, "text" : "IDX" } ) )
        self.labels["lblPos"]        = ( Label ( { "rect" : ( px+colWidth*3, tableY, 0, 0        ), "font" : self.font, "text" : "POS" } ) )
        self.labels["lblInt"]        = ( Label ( { "rect" : ( px+colWidth*4, tableY, 0, 0        ), "font" : self.font, "text" : "INT" } ) )
        self.labels["lblType"]       = ( Label ( { "rect" : ( px+colWidth*5, tableY, 0, 0        ), "font" : self.font, "text" : "TYPE" } ) )
        self.labels["lblGen"]        = ( Label ( { "rect" : ( px+colWidth*6, tableY, 0, 0        ), "font" : self.font, "text" : "GEN" } ) )
        self.labels["lblDel"]        = ( Label ( { "rect" : ( px+colWidth*7, tableY, 0, 0        ), "font" : self.font, "text" : "DEL" } ) )
        
           
    def Initialize( self ):
        # Click the window
        self.ResetLocation()
        
        if ( len( sys.argv ) >= 2 ):
            self.initializing = False
            
            
    def InitializeView( self ):            
        # Scroll to the top-left
        
        print( "Seconds:", self.GetElapsedSeconds() )
        if ( self.GetElapsedSeconds() >= 2 and self.GetElapsedSeconds() < 8 ):
            self.Log( "init", "Resetting view (H)..." )
            pyautogui.keyDown("a")
            
        elif ( self.GetElapsedSeconds() >= 8 and self.GetElapsedSeconds() < 11 ):
            self.Log( "init", "Resetting view (V)..." )
            pyautogui.keyUp("a")
            pyautogui.keyDown("w")
            
        elif ( self.GetElapsedSeconds() >= 11 and self.GetElapsedSeconds() <= 12.5 ):
            self.Log( "init", "Positioning view..." )
            pyautogui.keyUp("w")
            
            # move back to the town view
            pyautogui.keyDown("d")
        
        elif ( self.GetElapsedSeconds() > 12.5 ):
            self.Log( "init", "Done initializing..." )
            pyautogui.keyUp("d")
            self.initializing = False
            
    
    def SetMountPoint( self, position ):
        self.Log( "mount", "Set mount point..." )
        print( position )
        self.mountPoint = position
            

    def GetTotalSupplies( self ):
        return len( self.timingInfo["supplies"]["array"] )


    def GetTotalStocks( self ):
        return len( self.timingInfo["stocks"]["array"] )
        
        
    def GetTotalCoins( self ):
        return len( self.timingInfo["5mcoins"]["array"] ) + len( self.timingInfo["4hcoins"]["array"] )


    def PygameInit( self ):
        self.Log( "init", "Pygame init..." )
        pygame.init()
        self.fps = pygame.time.Clock()
        
        os.environ["SDL_VIDEO_WINDOW_POS"] = "%d,%d" % (self.windowX, self.windowY)
        self.window = pygame.display.set_mode( ( self.screenWidth, self.screenHeight ) )
        pygame.display.set_caption( "Game automate" )

        self.bgColor = pygame.Color( 150, 150, 200 )
        self.textColor = pygame.Color( 255, 255, 255 )
        self.highlightColor = pygame.Color( 255, 0, 0 )

        self.font = pygame.font.Font( "jozsika-regular.ttf", self.fontSize )
        self.pingSound = pygame.mixer.Sound( "ding.wav" )
        self.pingSound.set_volume( 0.1 )

        self.startTicks = pygame.time.get_ticks()

        self.Log( "init", "Starting ticks: " + str( self.startTicks ) )


    # From https://stackoverflow.com/questions/30720665/countdown-timer-in-pygame
    def GetElapsedSeconds( self ):
        seconds = ( pygame.time.get_ticks() - self.startTicks ) / 1000
        return seconds


    def GetElapsedMinutes( self ):
        minutes = int( self.GetElapsedSeconds() / 60 + self.timeOffset )
        return minutes


    def GetCurrentTicks( self ):
        return pygame.time.get_ticks()


    def DrawText( self, text, pos, highlight = False ):            
        if ( highlight ):
            messageSurface = self.font.render( text, False, self.highlightColor )
        else:
            messageSurface = self.font.render( text, False, self.textColor )
            
        messageRect = messageSurface.get_rect()
        messageRect.topleft = pos
        self.window.blit( messageSurface, messageRect )


    def LoadData( self ):
        self.Log( "load", "Load location data file..." )
        
        try:
            with open( "data.json", "r" ) as dataFile:
                self.timingInfo = json.load( dataFile )                
                
        except:
            self.ClearData()
            
        # Reset all "last collected" to 0.
        for timespan in self.timingInfo:
            for index in range( 0, len( self.timingInfo[timespan] ) ):
                self.timingInfo[timespan][index]["lastTriggered"] = 0
            
        self.BuildTableFromData()


    def SaveData( self ):
        self.Log( "save", "Save location data file..." )

        with open( "data.json", "w" ) as dataFile:
            json.dump( self.timingInfo, dataFile )


    def DryRun( self ):
        # Dry run
        self.Log( "dryrun", "Dry run..." )
        # for buildingType in self.timingInfo.items():
            # self.Log( "dryrun", buildingType )
            # print( buildingType )
            # if "array" in buildingType:
                # self.Log( "dryrun", str( len( buildingType["array"] ) ) + " buildings" )
                # for building in buildingType["array"]:
                    # self.MoveMouseAbsolute( building["absx"], building["absy"], 1 )


    def Log( self, category, text ):
        now = datetime.datetime.now()
        timestr = now.strftime( "%H:%M:%S" )
        
        minuteCount = 0
        if ( self.startTicks != None ):
            minuteCount = str( int( self.GetElapsedMinutes() ) )

        print( "(" + timestr + "/" + str( minuteCount ) + "m)\t", end="" )
        print( "[" + category.upper(), end="" )
        if ( len( category ) < 10 ):
            for i in range( 10 - len( category ) ):
                print( " ", end="" )
        print( "]\t", end="" )
        print( text, end="" )
        print()


    def MoveMouseAbsoluteAndClick( self, absX, absY ):
        self.MoveMouseAbsolute( absX, absY )
        self.ClickMouse()


    def MoveMouseAbsolute( self, absX, absY, speed = 0.5 ):
        pyautogui.moveTo( absX, absY, duration = speed )
        time.sleep( 0.5 )


    def ClickMouse( self ):
        self.Log( "click", "At position " + str( pyautogui.position() ) )
        pyautogui.click( pyautogui.position() )
        time.sleep( 0.5 )


    def GetMousePosition( self ):
        return pyautogui.position()




# -------------------------------------------------------------------- #
# BEGIN                                                                #
# -------------------------------------------------------------------- #
program = GameAutomater()
program.Run()

