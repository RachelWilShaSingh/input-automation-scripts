import pygame
from pygame.locals import *
import copy

class Label:
    def __init__( self, options ):
        self.rectangle      = ( 0, 0, 30, 30 )
        self.text           = "None"
        self.textColor      = pygame.Color( 20, 20, 70 )
        self.font           = None
        
        self.Setup( options )
        
    def Setup( self, options ):
        #print( "LABEL SETUP", options )
        if ( "rect" in options ):       self.rect = options["rect"]
        if ( "text" in options ):       self.text = options["text"]
        if ( "font" in options ):       self.font = options["font"]
        if ( "textColor" in options ):  self.textColor = options["textColor"]
        
    def Draw( self, window ):         
        messageSurface = self.font.render( self.text, False, self.textColor )
        messageRect = messageSurface.get_rect()
        messageRect.topleft = ( self.rect[0], self.rect[1] )
        window.blit( messageSurface, messageRect )
        

class Button:
    def __init__( self, options ):
        self.rect           = ( 0, 0, 300, 20 )
        self.textObject     = None
        self.action         = "None"
        self.bgColor        = pygame.Color( 185, 185, 225 )
        self.selColor       = pygame.Color( 255, 235, 145 )
        self.borColor       = pygame.Color( 255, 255, 255 )
        self.textColor      = pygame.Color( 20, 20, 70 )
        self.selected       = False
        
        self.Setup( options )
    
    def Setup( self, options ):
        #print( "BUTTON SETUP", options )
        if ( "rect" in options ):       self.rect = options["rect"]
        if ( "action" in options ):     self.action = options["action"]
        if ( "bgColor" in options ):    self.bgColor = options["bgColor"]
        if ( "selected" in options ):   self.selected = options["selected"]
        
        # Sub-objects
        if ( "text" in options ):
            optionsCopy = copy.copy( options )
            optionsCopy["rect"] = ( options["rect"][0] + 5, options["rect"][1] + 2, options["rect"][2] - 5, options["rect"][3] - 2 )
            self.textObject = Label( optionsCopy )
    
    def Draw( self, window ):
        color = self.bgColor
        if ( self.selected ): color = self.selColor
        
        # Draw button background
        pygame.draw.rect( window, color, self.rect )
        
        # Draw border
        pygame.draw.rect( window, self.borColor, self.rect, 1 )
        
        # Draw text
        if ( self.textObject != None ):
            self.textObject.Draw( window )
            
            
    def IsClicked( self, mousePos ):
        return ( mousePos[0] >= self.rect[0] and mousePos[0] <= self.rect[0] + self.rect[2] and mousePos[1] >= self.rect[1] and mousePos[1] <= self.rect[1] + self.rect[3] )
        
        
    def Toggle( self ):
        self.selected = not self.selected
        
    def Deselect( self ):
        self.selected = False
        
    def Select( self ):
        self.selected = True
